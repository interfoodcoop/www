module Jekyll
  class GitlabEditPageTag < Liquid::Block
    def render(context)
      repo_url = context.registers[:site].data.dig("company", "repository")
      page_path = context.registers[:page].fetch("path")
      repo = repo_url.split("/")
      project_path = repo[-2..-1].join("/")
      gitlab_url = repo[0..-3].join("/")

      text = super
      # Unfortunately Static Site Editor from Gitlab is only available for project members
      # with at least 'developer' access to the repo! At least as per the requirements written today
      # in https://docs.gitlab.com/ce/user/project/static_site_editor/#requirements
      # "<a href='#{repo_url}/-/sse/#{encode_path(page_path)}'>#{text}</a>"

      # Let's use the Web IDE for now
      "<a href='#{gitlab_url}/-/ide/project/#{project_path}/edit/master/-/#{page_path}'>#{text}</a>"
    end

    private

    # Unfortunately Static Site Editor from Gitlab is only available for project members
    # with at least 'developer' access to the repo! At least as per the requirements written today
    # in https://docs.gitlab.com/ce/user/project/static_site_editor/#requirements
    def encode_path(relative_path)
      ERB::Util.url_encode("master/#{relative_path}")
    end
  end
end

Liquid::Template.register_tag('gitlab_edit_page', Jekyll::GitlabEditPageTag)
