---
name: Cloud
subtitle: Share & store files online
order: 4
external_name: nuage
external_url: https://nuage.interfoodcoop.net/
image_path: /images/services/nuage.png
ref: nuage
lang: en
---
