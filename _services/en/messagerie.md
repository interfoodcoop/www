---
name: Messaging
subtitle: "Messaging"
order: 5
external_name: matrix
external_url: https://matrix.interfoodcoop.net/#/group/+intercoop:interfoodcoop.net
image_path: /images/services/matrix.svg
ref: messagerie
lang: en
---
