---
name: Nuage
subtitle: Stockage, partage et édition de fichiers
order: 4
external_name: nuage
external_url: https://nuage.interfoodcoop.net/
image_path: /images/services/nuage.png
ref: nuage
lang: fr
first_line: false
---
