---
name: Messagerie instantannée
subtitle: "La messagerie instantannée"
order: 5
external_name: matrix
external_url: https://matrix.interfoodcoop.net/#/group/+intercoop:interfoodcoop.net
image_path: /images/services/element.png
ref: messagerie
lang: fr
first_line: false
---
