---
layout: presentation
title: Charte d’éthique
permalink: charte/
ref: charte
lang: fr
---

## 1. Introduction

Nous appliquons cette charte d’éthique aux projets auxquels nous participons.

Il s’agit d’idéaux, d’objectifs à atteindre, pas d’une liste à cocher. Nous sommes faillibles, et ce texte est imparfait. Nous sommes conscient·e·s que cette charte ne répond pas à la complexité de toutes les questions éthiques, et nous nous engageons à questionner nos pratiques, et à adapter cette charte à mesure.

## 2. De quoi parle-t-on ?

### 2.1. Une coopérative alimentaire

C’est une structure de distribution participative et autogérée, à but non lucratif, où les membres contribuent bénévolement à hauteur de 3 heures par mois. Elles ou ils en sont les seuls propriétaires, les seuls décisionnaires et les seuls clients. La coopérative peut être organisée sous forme d’association, société par actions simplifiées (SAS), société coopérative de production (Scop), société coopérative d’intérêt collectif (Scic), …

La coopération entre les personnes ainsi que la participation des membres est encouragée, chacun apportant son savoir et son temps en fonction de ses possibilités. La solidarité entre les membres, avec les fournisseurs ou encore avec les associations locales, via par exemple une caisse d’aide en cas de besoin, des collectes alimentaires, des initiatives solidaires, est aussi, souvent, au cœur du projet.

### 2.2. Contenus et outils mis à disposition

L’objet de ce collectif est d’apporter de l’aide (temps humain et des outils numériques), des conseils et des ressources pour faciliter la vie numérique des coopératives en création ou en fonctionnement.

Notamment par l’organisation de réunions régulières pour animer le collectif. Celles-ci ayant pour but de proposer des retours d’expériences aux coopératives, sur la méthodologie, la gouvernance, la vision et la gestion de projet qui y sont abordés et documentés. La documentation produite est dans la majorité des cas en libre accès et libre de droits.

L’élaboration et la collaboration sur du code informatique, open-source[^open_source] (« recette de cuisine » publique) et libre[^libre] (code non soumis à des droits de propriétés privées) est également un des objectifs du collectif. Afin de mettre à disposition aux coopératives le souhaitant du code réutilisable et potentiellement intégrable à leurs outils numériques.

Pour favoriser l’entre-aide entre les coopératives, des outils numériques libres sont disponibles et mis à disposition par le collectif, notamment :

* <https://www.interfoodcoop.net/> : le site vitrine qui présente la démarche, informe et oriente vers les services disponibles.
* <https://forum.supermarches-cooperatifs.fr/> : Un lieu d’échange pour construire le futur des communs et la vision partagée des projets en cours. **C’est le lieu principal d’animation et de communication de la communauté.** On peut considérer cet espace comme étant la place publique où tout le monde peut participer ou tout simplement écouter.
* <https://element.interfoodcoop.net/> : La messagerie instantanée « Element », basée sur le protocole Matrix fournit la discussion en temps réel, l’échange de fichiers et les appels en visioconférences. Son système de fédération permet de se connecter et de communiquer entres coopératives. La messagerie est assimilable, dans le monde réel, au café du coin.
* <https://wiki.supermarches-cooperatifs.fr/> : de la documentation générale sur les fonctionnements internes des supermarchés coopératifs, partagée sous licence libre ([CC by-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)). Ici c’est la bibliothèque qui regroupe des partages de connaissances.
* <https://guides.interfoodcoop.net/> : Des guides utilisateurs pour bien démarrer avec des outils déjà éprouvés par plusieurs coopératives, également partagés sous licence libre ([CC by-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)).
* <https://nuage.interfoodcoop.net/> : Un espace de partage de fichiers, NextCloud, permet d’accéder aux dossiers et documents partagés, aux agendas, aux carte de suivis des projets. On peut aussi y créer des formulaires ou des sondages.

### 2.3. Les numériques

Comment peut-on associer les valeurs humaines que l’on souhaite voir se propager non seulement dans les aspects physiques de nos vies mais aussi dans les espaces numériques qui nous entourent au jour le jour ?

Un des objectif du collectif Interfoodcoop est d’essayer d’apporter des réponses concrètes à cette question via l’émancipation de ces membres par l’usage de logiciels numériques libres et respectueux de leur vie privée. Et via également l’usage de systèmes qui tentent d’être un maximum intéropérables[^interop] pour ne pas enfermer les utilisateurs dans tel ou tel solution.

Les espaces numériques disponibles sont nombreux mais bien souvent contrôlés par des entreprises géantes, à la recherche d’une croissance économique faramineuse, qui fournissent ses services où les utilisateurs n’ont plus de choix et doivent s’abreuver d’un usage qu’ils n’ont bien souvent pas décidé.

Le collectif essaye donc de sensibiliser sur les dangers de la centralisation des outils numériques offerts par les géants de l’internet qui sont très souvent des **logiciels privateurs**[^privateur] non émancipateur et incontrôlable.

Nous souhaitons également rappeler l’importance d’être en contrôle de ses données personnelles, de ne pas les fournir gratuitement à des acteurs ne respectant pas la **vie privée** ou ayant un but lucratif sur des données qui vous appartiennent.

## 3. Règles de bonne conduite

Nous promouvons le respect, l’entraide et la solidarité, ainsi que l’écoute et la bienveillance. Nous tâchons de veiller à ce que chacune et chacun puisse s’exprimer, proposer des idées et incitons à l’épanouissement personnel par la réalisation et l’expérimentation.

Le droit à l’erreur ainsi que le droit à l’oubli sont essentiels à nos yeux. Aussi nous accordons de l’importance à l’inclusivité, à l’autonomie, à la politesse et à la gentillesse.

* on reste calme et respectueux ;
* en cas de tension, on pose son clavier, on va boire une tisane et on revient répondre calmement à tête reposée ;
* tout le monde a le droit de s’exprimer ;
* on ne partage rien d’illégal ou d’inapproprié ;
* le but étant qu’ici tout le monde se sente à l’aise, on vous demande de faire attention à vos propos de façon à ne heurter personne.

## 4. Nos valeurs et principes

Le collectif InterFoodcoop souhaite partager certaines valeurs qui se doivent d’unir ses membres et les utilisateurs des services proposés.

* **Nous souhaitons utiliser des solutions dont le code source[^source_machine] est publié en tant que logiciels libres[^libre]** et aussi favoriser leur essaimage, leur utilisation et leur démocratisation dans la communauté des coopératives alimentaires et autres.
* **Nous souhaitons favoriser l’utilisation d’outil numériques décentralisés et fédérés.** \
  *Nous pensons que ces outils permettent d’éviter l’accumulation d’informations par une même entreprise, une même personne physique ou morale. Ils ont par ailleurs vocation à permettre à chaque entité de communiquer entre elles sans avoir besoin de mettre en place une entité centrale. Ces outils favorisent donc le partage libre d’information à travers des projets communs distincts.*
* **Nous souhaitons favoriser l’accès et l’inclusivité des services proposés au plus grand nombre.** \
  *Nous choisissons des solutions logiciels libres accessibles à tous grâce à des interfaces simples, fonctionnelles et intuitives. Nous tentons d’éviter la multiplication des outils aux fonctions similaires pour ne pas nuire à l’inclusivité et à l’appropriation des services par tous.*
* **Nous souhaitons utiliser des outils numériques qui respectent la vie privée de leurs utilisatrices et utilisateurs.** \
  *Les données privées collectées ne feront pas l’objet d’une commercialisation. Les utilisateurs des services proposés peuvent exiger la récupération de leur données personnelles, chiffrées ou non, sauf dans le cas de services particuliers reposant sur le transfert éphémère et chiffrés d’informations personnelles.*
* **Nous nous assurons que nos services ne sont pas hébergés par des acteurs qui ne favorisent pas la neutralité du réseau internet.** \
  *Nous nous assurons que le transit des paquets de données est fait sans discrimination et sans éxamination de leur contenu, leur source ou leur destination. Nous ne privilégions aucun mode de distribution des informations. Nous n’altérons arbitrairement aucune donnée, ni contenu et nous nous engageons, autant que nos moyens le peuvent, à rapporter à nos utilisateurs toute entrave externe constatée à ces principes de neutralité.*
* **Les administrateurs des services s’engagent à ne pas examiner, à ne pas censurer ou sanctionner le contenu partagé par les utilisateurs de nos services** dans la mesure où il n’outrepasse pas le cadre législatif en vigueur (ex : si un partage illégal de contenu nous est rapporté par un utilisateur ou une autorité publique, les administrateurs pourront le supprimer).
* **Pour éviter tout désagrément dans nos espaces partagés de communication (ex : sur le forum[^forum] ou la messagerie instantanée), nous faisons appels à des modérateurs parmi les utilisateurs de nos services.** \
  *Ces modérateurs ont toute autorité pour filtrer les contenus et sanctionner des comportements qu’ils ou elles jugeraient inapproprié par rapport à cette présente charte. Tout filtre ou sanction suivra une discussion entre les personnes impliquées et le(s) modérateurs.*
* **Nous souhaitons proposer des services mis à disposition gracieusement dans le respect de la juridiction Française, dont l’accès est attribué sans aucune discrimination.** \
  *Pour le moment nous n’avons pas les moyens financiers et humains de fournir ces outils à l’ensemble des membres d’une coopérative ou d’un collectif. Nous invitons donc les utilisateurs à limiter leur usage au partage d’informations pour la communauté de l’InterFoodcoop et à l’expérimentation en petit commité (e.g. : entre quelques membres d’un groupe informatique d’une coopérative alimentaire).*
* **Nous n’acceptons aucune publicité en provenance de régies publicitaires.**
* **Nous n’avons pas vocation à inciter nos utilisateurs à se tourner vers un prestataire de services plutôt qu’un autre** et invitons sans discrimination toute entreprise, association ou coopérative à se joindre au projet pour présenter leurs outils et leurs compétences.

## 5. Organisation interne et gouvernance

Association « de fait » (ou « non déclaré »), nous sommes un groupement de personnes (physiques et morales) qui n’avons pas souhaité accomplir les formalités de déclaration.

Le collectif est organisé de façon relativement **organique**, la gouvernance des décisions est portée par des discussions ouvertes qui naissent dans **le forum** et dans **les réunions hebdomadaires** qui se déroulent en visioconférence. Les décisions sont donc soumises à des choix portés par chacun des groupes de travail et peuvent continuellement être remise en question, ou adaptées. Ceci dans le respect et l’application de cette charte (notamment les paragraphes 3 et 4 de bonne conduite et de valeurs).

Toute décision structurante du collectif et choix des projets en cours ou à venir sont fait en groupe lors des réunions du collectif. Et les resultats sont partagés grâce aux [comptes-rendus écrits et publiquement accessibles](https://nuage.interfoodcoop.net/s/sRbQBfQ2j2fkjkT). Tout en gardant à l’esprit, de part le fonctionnement très asynchrone du collectif, que chaque personne impliquée dans le projet est libre d’émettre un avis argumenté sur des décisions passés pour en améliorer le résultat en adéquation avec cette présente charte.

Financièrement les **dépenses** sont avancés, à la fois par quelques coopératives (en gouvernance alternés année après année, initialement porté par « La Chouette », et en 2020 porté par « La Cagette ») et aussi par le collectif InterFoodCoop (pour l’instant dépenses faites par Paul B.). \
À titre indicatif, les dépenses du collectif sur le dernier trimestre 2020 sont visibles dans [ce document](https://nuage.interfoodcoop.net/s/f6tbxfmSJCgxbe9).

Les **recettes** sont, pour les coopératives, obtenues sur les revenus usuels de leur fonctionnement, et pour le collectif InterFoodCoop, elles sont obtenues par des dons sur la [plateforme liberapay](https://liberapay.com/InterFoodCoop).


## Glossaire

*(Certaines définitions copiés de <http://guide.libreassociation.info/html/> sous [licence CC by-sa 2.0](http://guide.libreassociation.info/html/#htoc65))*

[^forum]: **Forum**

    *(Figuré)* Lieu où se discutent les affaires publiques.

    (*Internet*) Service permettant des discussions et échanges sur un thème donné où chaque utilisateur peut lire à tout moment les interventions de tous les autres et apporter sa propre contribution sous forme d’articles.

[^interop]: **Interopérabilité**

    L’interopérabilité est la capacité que possède un produit ou un système, dont les interfaces sont intégralement connues, à fonctionner avec d’autres produits ou systèmes existants ou futurs, et ce sans restriction d’accès ou de mise en œuvre. En informatique, l’interopérabilité repose en grande partie sur des standards ouverts.

[^privateur]: **Logiciel privateur**

    La notion de **logiciel privateur**, aussi appelé logiciel propriétaire, s’oppose à celle du **logiciel libre**, en ce sens qu’il prive l’utilisateur de ses libertés.

[^libre]: **Logiciel libre**

    Un logiciel est dit libre s’il est disponible sous une licence qui protège et assure les quatre libertés fondamentales à ses utilisateurs :
    * La **liberté de l’utiliser** sans contraires, sans restrictions ;
    * La **liberté de l’étudier**, de pouvoir lire son code source[^source_machine] pour comprendre ce que le logiciel fait exactement ;
    * La **liberté de l’améliorer**, et de le modifier à sa guise ;
    * La **liberté de le partager**, et de le redistribuer à quiconque.

[^source_machine]: **Code source et code machine**

    Les logiciels sont écrits dans des langages facilement compréhensibles par les humains : le code source. En revanche, les machines manipulent des instructions peu compréhensibles : le code machine (dit encore code binaire). On passe généralement de l’un à l’autre par une opération de traduction irréversible. Cette impossibilité de retrouver le source à partir du code binaire est à l’image d’un gâteau cuisiné à partir d’ingrédients distincts : une fois cuit, il est impossible de le « décuire ».

[^open_source]: **Code source ouvert**

    Un code source qui décrit le fonctionnement d’un logiciel **est ouvert** si toutes ses lignes de codes sont disponibles à la lecture publiquement. À l’image d’un gâteau, sa recette de cuisine est connue et partagée publiquement.
