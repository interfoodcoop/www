---
title: InterFoodcoop
layout: default
permalink: /
ref: index
lang: fr
---

{% section hero %}

## Des communs pour nos supermarchés coopératifs !

InterFoodcoop.net promeut et développe l'utilisation d'outils libres et open-sources dont les valeurs sont si proches de nos [supermarchés coopératifs et participatifs](https://supermarches-cooperatifs.fr/).

InterFoodcoop.net regroupe des bénévoles, des associations, des membres de plusieurs supermarchés pour développer des logiciels en communs pour simplifier leur gestion informatique.

{% endsection %}

{% section patterned|halves|left %}

### Une seule porte d'entrée

Il suffit de nous rejoindre sur le forum. Une fois votre compte créé vous pourrez partager, discuter, documenter grâce à tous les outils suivants mis à votre disposition.

[Le forum](https://forum.supermarches-cooperatifs.fr/){:.button .alt}

{% endsection %}

{% section alternate %}

### Nos outils communs

Un forum de discussion

<ul class="image-grid">
{% assign _services = site.services | where: "lang", "fr" | where: "first_line", true | sort: 'order' %}
{% for service in _services limit: 1 %}
    <li>
        <a href="{{ site.baseurl }}{{ service.external_url }}"><img src="{% include relative-src.html src=service.image_path %}" class="screenshot">
            <div class="details">
                <div class="position">{{ service.subtitle }}</div>
            </div>
        </a>
    </li>
    <li></li>
    <li></li>
    <li></li>
{% endfor %}
</ul>

…qui permet d'accéder à des outils décentralisés utilisables par toutes et tous

<ul class="image-grid">
{% assign _services = site.services | where: "lang", "fr" | where: "first_line", false | sort: 'order' %}
{% for service in _services limit: 6 %}
    <li>
        <a href="{{ site.baseurl }}{{ service.external_url }}"><img src="{% include relative-src.html src=service.image_path %}" class="screenshot">
            <div class="details">
                <div class="position">{{ service.subtitle }}</div>
            </div>
        </a>
    </li>
{% endfor %}
</ul>

{% endsection %}

{% section secondary|halves|custom %}

<div markdown="1">

### Nos rencontres régulières

Nous _essayons_ de nous retrouver, avec ceux qui le souhaitent, de façon bi-hebdomadaire pour des rencontres en ligne de 1 ou 2 heures [en général les jeudis soirs](https://nuage.interfoodcoop.net/apps/calendar/p/yy7zSzE6ibYK2Kaq).

Le but de ces rencontres est à la fois pour l'organisation du collectif, pour discuter des sujets en cours et aussi pour partager les dernières nouvelles.

[Voir les derniers comptes-rendus](https://nuage.interfoodcoop.net/s/sRbQBfQ2j2fkjkT){:.button .alt}{:type=blue}

[Voir les derniers enregistrements vidéos](https://video.antopie.org/a/collectif_interfoodcoop/videos?s=1){:.button .alt}{:type=blue}

</div>

<div class="hiddenOnMobile">
    <ul class="image-list">
        <li><img src="{{ site.baseurl }}/images/bbb-audio.png" width="400"></li>
    </ul>
</div>


{% endsection %}

{% section alternate|halves|custom %}

<div class="hiddenOnMobile">
    <ul class="image-list">
        <li><img src="{{ site.baseurl }}/images/small-strawberries.jpg" width="400"></li>
    </ul>
</div>

<div markdown="1">

### Comment nous aider ?

Tu es coopératrice ou coopérateur, informaticienne ou informaticien, consommatrice ou consommateur responsable ? Tu as du temps ? Intéressé.e par notre démarche ?

[Rejoins-nous sur le forum !](https://forum.supermarches-cooperatifs.fr/t/communaute-de-developpeurs-autour-de-logiciels-libres/1800){:.button .alt}{:type=blue}

</div>

{% endsection %}
