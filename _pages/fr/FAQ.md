---
layout: presentation
title: À propos (FAQ)
permalink: FAQ/
ref: faq
lang: fr
---

**Foire Aux Questions** InterFoodcoop, à destination des projets de
supermarchés coopératifs intéressés par les ressources communes
disponibles ici !

## InterFoodcoop, qu'est-ce que c'est ? Et qui ?

Un collectif de personnes intéressées par la mutualisation des outils et
ressources informatiques à destination des coopératives alimentaires au
sens large. Dans ce collectif, il y a de simples bénévoles et des
salarié.e.s de supermarchés coopératifs, des entreprises du libre ou des
associations, des programmeurs.euses chevronné.e.s comme des petit.e.s
bidouilleurs.euses, bref ! La diversité du collectif est à l'image de
celles des cultures de nos différents projets.

## Pourquoi InterFoodcoop ?

Parce que beaucoup de projets débutants, ou même avancés, sont à la
recherche de solutions informatiques fiables pas trop onéreuses en
accord avec leurs valeurs ! En effet, même si des outils numériques
performants sont, de nos jours, un des axes les plus importants du
développement d'un projet coopératif, leur coût est très souvent
sous-estimé, et on a pu observer, au premier chef dans nos propres
projets, que ça pouvait mener à des difficultés ou des surprises
désagréables... on essaie donc de partager des informations et
des retours d'expérience à propos des outils fonctionnels et de leur
spécificité pour guider aussi bien les débutants que les spécialistes.

## Quelles sont vos valeurs ?

Il y a déjà un consensus sur quelques éléments incontournables
pour nous : la promotion et l'utilisation de logiciels
libres, le libre partage de l'information, l'entraide, le respect de la
vie privée des utilisateurs, l'inclusivité, …

Pour plus de détails sur les valeurs que nous souhaitons promouvoir,
soutenir et entretenir nous t'invitons à lire [notre charte](/charte).

## InterFoodcoop, ça consiste en quoi, au juste ?

Eh bien à se filer des coups de main, surtout ! Puis à fournir des outils pour
rassembler et organiser toutes les énergies et les infos utiles concernant les
projets de coopératives alimentaires.
Pour ça, comme tu le verras sur la page d'accueil, on a déjà un certain
nombre d'outils :

- le [**forum**](https://forum.supermarches-cooperatifs.fr/){:target="_blank"} (très fréquenté) ;
- le [**wiki**](https://wiki.supermarches-cooperatifs.fr/){:target="_blank"} (un peu moins
fréquenté, ce qui est dommage ! Si tu vas y chercher des ressources,
n'oublie pas d'en ajouter !) ;
- un [**stockage de fichiers**](https://nuage.interfoodcoop.net/){:target="_blank"} ;
- des [**guides d'utilisation**](https://guides.interfoodcoop.net/){:target="_blank"} ;
- une [**messagerie instantanée**](https://matrix.interfoodcoop.net/#/group/+intercoop:interfoodcoop.net){:target="_blank"} ;
- et bientôt un git…

N'hésite pas à faire un tour dans chacune de ces
ressources pour les découvrir. Tu peux pour cela utiliser ton compte du
forum ou en créer un pour accéder à tous les autres outils.

L'objectif final est pour nous d'apporter une aide pour que les projets de
supermarchés coopératifs puissent y voir clair et avancer efficacement,
notamment sur les enjeux techniques.

## À qui s'adressent les services de l'InterFoodcoop ?

Les outils et services proposés par l'InterFoodcoop s'adressent à toutes
les personnes qui peuvent y voir un intérêt dans le cadre du développement 
de leur coopérative alimentaires ou autres. 
Pour le moment nous n'avons pas les moyens financiers ([**n'hésite pas à les soutenir**](https://liberapay.com/InterFoodcoop/){:target="_blank"}) 
et humains de fournir ces outils directement à l'ensemble des membres d'une coopérative. Vous pouvez 
néanmoins vous en emparer et faire des essais en expérimentant ces outils libres
et fédérés avec la communauté de l'InterFoodcoop. Vous pouvez aussi utiliser ces
outils dans un cadre restreint à quelques utilisateurs, par exemple entre des membres 
de votre « groupe informatique ».

## Ça coûte combien ?

Les coûts d'hébergement de certains outils sont mutualisés de façon
informelle et temporaire entre plusieurs coopératives (Montpellier et
Toulouse au premier chef), d'autres sont mis en place gracieusement par
des âmes charitables ([**n'hésite pas à les aider**](https://liberapay.com/InterFoodcoop/){:target="_blank"}). **Du coup c'est gratuit et ça le restera à tout jamais.** On ne s'interdit pas de
réfléchir au financement participatif d'un emploi dédié. Mais pour que
ça ait un sens, pardon si on se répète, il faut que l'on ait assez de
participations volontaires pour faire avancer les choses en
collaboration.

Un [tableau des dépenses](https://nuage.interfoodcoop.net/s/f6tbxfmSJCgxbe9){:target="_blank"}
(purement informatif) pour fin 2020 est publié
sur le stockage de fichiers partagés et peut donner une idée aux
curieux de l'investissement en cours.

Un autre tableau de comptabilité, concernant uniquement les dépenses et dons « Interfoodcoop »,
est calculé automatiquement à chaque modification du présent site web. [Voir le résultat au 1 août 2021](https://gitlab.com/interfoodcoop/www/-/jobs/1468183247).

## Ça m'intéresse, je veux aider, mais où aller ? Qui contacter ?

Le mieux c'est de commencer par explorer un peu le forum et les
différents outils, puis de prendre contact avec le collectif via la
messagerie (dans le salon
[**#Info-interfoodcoop**](https://matrix.interfoodcoop.net/#/room/#Info-interfoodcoop:interfoodcoop.net){:target="_blank"}),
et enfin de participer à l'une des réunions organisées régulièrement. Tu trouveras plus
d'information sur ces réunions, ainsi que leur compte-rendu que nous mettons à
disposition publiquement dans [ce dossier partagé](https://nuage.interfoodcoop.net/s/sRbQBfQ2j2fkjkT).

## Est-ce que InterFoodcoop a quelque chose à voir avec la Louve, « Awesome Food Coops » ou « Participative Food Coops Alliance » ?

Oui, en quelque sorte, enfin, à vrai dire, non, mais on aimerait
bien... nos camarades et glorieux prédécesseurs parisiens de [_la Louve_](https://cooplalouve.fr/) ont en effet fait le choix de réserver leur
« essaimage » ou aide au développement aux projets qui adhèrent sans
restriction au modèle (et à l'environnement logiciel) « Awesome Food
Coops ». Tom et Brian ont en outre depuis longtemps le dessein de
proposer une plate-forme de développement coopératif pour les projets de
supercoops, « Participative Food Coops Alliance », mais on ne peut pas
t'en dire beaucoup à ce sujet : on a essayé à de multiples reprises au
cours des années 2019 et 2020 de leur proposer des rencontres ou
réunions sur l'informatique intercoops mais ça ne s'est jamais fait.
Donc en attendant, de nombreuses coops se sont organisées pour collaborer et nous
souhaitons concrétiser ces efforts avec l'InterFoodcoop !

## Et avec l'Intercoop ? Il y a un lien ?

Si tu poses la question, c'est que tu es courant de l'existence des
[**rencontres intercoop**](https://rencontres.supermarches-cooperatifs.fr/). Excellent
! Oui, il y a un lien très net, c'est en se retrouvant et en échangeant
régulièrement entre nous que le collectif s'est monté, même si sa
formalisation à travers ce site date des toutes dernières intercoops
dématérialisées en 2020. [InterFoodcoop.net](http://interfoodcoop.net)
est donc un collectif né directement des initiatives de
[supermarches-cooperatifs.fr](http://supermarches-cooperatifs.fr).

## J'ai une autre question ?

Envoie-nous la ! Soit sur [le](https://forum.supermarches-cooperatifs.fr)
forum, soit sur [la](https://matrix.interfoodcoop.net/#/room/#Info-interfoodcoop:interfoodcoop.net)
messagerie
ou alors [par email](mailto:contact@interfoodcoop.net) tout simplement.
On y répondra ici même ;)
