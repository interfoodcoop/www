---
layout: presentation
title: Mentions Légales
permalink: mentions-legales/
ref: mentions-legales
lang: fr
---


Ce site web est édité et maintenu de façon coopérative au sein de la communauté [forum.supermarches-cooperatifs.fr](https://forum.supermarches-cooperatifs.fr/). À ce jour [et temporairement] les mentions légales sont attribuées à Paul Bonaud, contactable à [paul [at] bonaud.fr](mailto:paul at bonaud.fr).

## Éditeurs et contenu de ce site web

Ce site web est maintenu publiquement sur [gitlab.com/interfoodcoop](https://gitlab.com/interfoodcoop/www). Le code source du site est ouvert et disponible sous licence GNU GPLv3. Le contenu du site est publié sous licence [Creative Commons de type CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en_US).

Les éditeurs du site sont disponibles en listant les contributeurs du dépot `interfoodcoop/www` par [ici](https://gitlab.com/interfoodcoop/www/-/graphs/master).

## Hébergement du site web

Les pages de ce site web sont actuellement déployés et hébergés via le service [Gitlab.com](https://gitlab.com/).

## Hébergement des services interfoodcoop

Tous les services de l'[interfoodcoop](https://www.interfoodcoop.net) sont déployés et maintenus de manière transparentes et responsables grâce à une configuration 100 % programmatique et dont le code est visble publiquement sur [https://gitlab.com/interfoodcoop/infra](https://gitlab.com/interfoodcoop/infra).

L'hébergeur actuel de ces services est la société Allemande « Hetzner » :

Hetzner Online AG
Stuttgarter St. 1
91710 Gunzenhausen
Germany

Tél. : +49 – 9831 - 610061
Site Web : [www.hetzner.com](https://www.hetzner.com)

## Déclaration à la CNIL

Conformément à la loi 78-17 du 6 janvier 1978 (modifiée par la loi 2004-801 du 6 août 2004 relative à la protection des personnes physiques à l’égard des traitements de données à caractère personnel) relative à l’informatique, aux fichiers et aux libertés, ce site n’a pas fait l’objet d’une déclaration auprès de la Commission nationale de l’informatique et des libertés (www.cnil.fr).

La [politique de confidentialité](/politique-confidentialite#politique-de-confidentialité-applicable-au-site-web-et-à-lensemble-des-services-interfoodcoopnet) et les [conditions d'utilisation](/politique-confidentialite#conditions-dutilisation) du site web et des services attachés sont disponible sur [une page dédié](/politique-confidentialite).
