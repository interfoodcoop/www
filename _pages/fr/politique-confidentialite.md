---
layout: presentation
title: Politique de confidentialité & conditions d'utilisation
permalink: politique-confidentialite/
ref: politique-confidentialite
lang: fr
---


## Politique de Confidentialité applicable au site web et à l'ensemble des services interfoodcoop.net

Conformément à l’article 34 de la loi « Informatique et Libertés », vous avez un droit d’opposition, d’accès et de rectification sur les données nominatives vous concernant, par tout moyen de contact valide selon la définition donnée dans les [mentions légales](/mentions-legales) de ce site.

Comme le requiert la loi française, certaines informations vous identifiant sont enregistrées automatiquement lors de l’usage des services, incluant notamment l’adresse IP et les heures de connexion aux services.

Toutes les informations vous concernant seront utilisées uniquement en interne pour permettre d’assurer une utilisation correcte des services, et ne seront ni revendues ni transmises à des tiers, en dehors des éventuelles requêtes juridiques correctement justifiées.

## Conditions d'utilisation

InterFoodcoop.net met à disposition ce site web et des services pour la communauté des supermarchés coopératifs et participatifs de France afin de pouvoir discuter de projets communs, de s'entraider et de participer à la construction d'un future plus respectueux de chacune et de chacun d'entre nous.

Ces services sont gratuits et disponibles pour toutes les personnes qui font partie de la communauté sur forum.supermarches-cooperatifs.fr. Les règles de modérations de contenus sont donc directement liés à celles du forum.

L’utilisateur s’engage notamment, dans ses échanges avec les autres utilisateurs, à respecter les règles usuelles de politesse et de courtoisie.

L’utilisateur est seul responsable des contenus de toute nature (rédactionnels, graphiques, audiovisuels ou autres, ainsi que la dénomination et/ou l’image éventuellement choisies par l’utilisateur pour l’identifier sur le site) qu’il diffuse.
