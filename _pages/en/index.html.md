---
title: InterFoodcoop
layout: default
permalink: en/
ref: index
lang: en
---

{% section hero %}

## For commons in our FoodCoops!

InterFoodcoop.net promotes and develops the use of free and open-source software for supermarket food cooperatives whose values are so aligned.

InterFoodcoop.net brings together activists, volunteers, members of multiple food cooperatives to develop commons as software to simplify the work of supermarkets.

{% endsection %}

{% section patterned|halves|left %}

### A single entrypoint

You need to join us on the forum. Once your account is created you will be able to share, discuss, document thanks to all the tools made available for the community.

[The forum](https://forum.supermarches-cooperatifs.fr/){:.button .alt}

{% endsection %}

{% section alternate %}

### Our common tools

Decentralised hosting of free software tools usable by all

<ul class="image-grid">
{% assign _services = site.services | where: 'lang', 'en' | sort: 'order' %}
{% for service in _services limit: 6 %}
    <li>
        <a href="{{ site.baseurl }}{{ service.external_url }}"><img src="{% include relative-src.html src=service.image_path %}" class="screenshot">
            <div class="details">
                <div class="position">{{ service.subtitle }}</div>
            </div>
        </a>
    </li>
{% endfor %}
</ul>

{% endsection %}

{% section hero secondary|halves|custom %}

<div class="hiddenOnMobile">
    <ul class="image-list">
        <li><img src="{{ site.baseurl }}/images/small-strawberries.jpg" width="400"></li>
    </ul>
</div>

<div markdown="1">

### How to help us?

You are a cooperator in a FoodCoop, a developer, a responsible consumer? You have a bit of spare time? Interested by the initiative?

[Join us on the forum!](https://forum.supermarches-cooperatifs.fr/){:.button .alt}{:type=blue}

</div>

{% endsection %}
